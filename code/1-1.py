import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
f = 100
T = 1/f
x = np.arange(0,3 * T, 0.00001)
y = signal.square(2 * np.pi* f * x)
plt.figure(1)
plt.plot(x,y,linewidth=0.8)
plt.ylim(-2,2)
plt.xlabel('$t$')
plt.ylabel('$f(t)$')
plt.grid(True)
plt.show()


def fourier(e):
    x = np.arange(0,3 * T, 0.00001)
    y = 0 * x
    for i in range(1,e+1):
        if i % 2 == 1:
            y = y + (4/np.pi)*(1/i)*np.sin(i * 1/T * 2 * np.pi * x)
    return y


def paint(n):
    plt.plot(x,fourier(n_max[n]),linewidth=0.8)
    plt.plot(x,y,linewidth=0.8)
    plt.xlabel('$t$')
    plt.ylabel('$f(t)$')
    plt.title('$n = {}$'.format(n_max[n]))
    plt.ylim(-1.5,1.5)
    plt.grid(True)

plt.figure(2)
n_max = [1,10,100]
N = len(n_max)
plt.subplot(131)
paint(0)
plt.subplot(132)
paint(1)
plt.subplot(133)
paint(2)


plt.show()
